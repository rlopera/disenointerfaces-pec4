using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CoffinCoverManager : MonoBehaviour
{
   [SerializeField] int ID;
   public Action<int> OnEnter;
   public Action<int> OnExit;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter(Collider other) {
		if(other.CompareTag("SkeletonSkull")) {
         OnEnter(ID);
		}
	}

   private void OnTriggerExit(Collider other) {
      if (other.CompareTag("SkeletonSkull")) {
         OnExit(ID);
      }
   }
}
