using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour
{

   public GameObject GOLight;
   public float multiplier;
   [Range(1f, 2f)] public float initialValue;


   private Light _light;
   private bool _lightmode; //false=down, true=up

    // Start is called before the first frame update
    void Start()
    {
      _light = GOLight.GetComponent<Light>();
      _lightmode = false;
      _light.intensity = initialValue;
    }

    // Update is called once per frame
    void Update() {
      if(_lightmode == false) {
         _light.intensity -= Time.deltaTime*multiplier;
         if(_light.intensity < 1f) {
            _lightmode = true;
			}
		} else {
         _light.intensity += Time.deltaTime*multiplier;
         if (_light.intensity > 2f) {
            _lightmode = false;
         }
      }    
    }
}
