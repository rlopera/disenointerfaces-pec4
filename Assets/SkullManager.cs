using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SkullManager : MonoBehaviour
{

   public Action OnSelect;
   public Action OnDeselect;
	public void OnSelectEnter() {
      // Aqu� puedes agregar la l�gica que desees cuando el objeto es seleccionado
      Debug.Log("Objeto ENTER seleccionado con el rayo del simulador");
      OnSelect();
      // Por ejemplo, puedes ejecutar una animaci�n, activar/desactivar algo, etc.
      
   }

   public void OnSelectExit() {
      // Aqu� puedes agregar la l�gica que desees cuando el objeto es seleccionado
      Debug.Log("Objeto EXIT seleccionado con el rayo del simulador");
      OnDeselect();
      // Por ejemplo, puedes ejecutar una animaci�n, activar/desactivar algo, etc.
   }
}
