using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

   public GameObject[] Covers;
	public GameObject SkeletonSkull;
	//public GameObject[] CrossText;
	public GameObject DeathEffect;

	private int SkullIntoZone;
	private bool SkullSelect;
	private bool IsSelected;

	// Start is called before the first frame update
	void Awake() {
      foreach(GameObject go in Covers) {
			CoffinCoverManager ccm = go.GetComponent<CoffinCoverManager>();
			ccm.OnEnter += OnEnterCoffin;
			ccm.OnExit += OnExitCoffin;
		}
		SkullManager sm = SkeletonSkull.GetComponent<SkullManager>();
		sm.OnSelect += OnSelectSkull;
		sm.OnDeselect += OnDeselectSkull;

		SkullIntoZone = -1;
		SkullSelect = false;
		IsSelected = false;
   }

	// Update is called once per frame
	void Update() {
		if (SkullIntoZone != -1 && !IsSelected) {	//est� dentro de una zona
			if (SkullSelect) {      //la calavera est� agarrada
											//en este momento se puede mostrar m�s ayuda al jugador
				Debug.LogWarning("MOSTAR M�S AYUDA PORQUE EST� CERCA");
			} else {                //la calavera no est� agarrada
											//en este momento se ha seleccionado la opci�n del men�
				Debug.LogWarning("SE HA SELECCIONADO LA OPCI�N DEL MEN�");
				//CrossText[SkullIntoZone - 1].SetActive(true);
				//Covers[SkullIntoZone - 1].transform.parent.transform.Rotate(0f, 45f, 0f);
				IsSelected = true;

				if (SkullIntoZone < 4) {	//coffins
					GetComponent<AudioSource>().Play();
					StartCoroutine(MoveCoffin(Covers[SkullIntoZone - 1].transform.parent, 45));
				} else { //exit option
					DeathEffect.GetComponent<ParticleSystem>().Play();
					Covers[SkullIntoZone - 1].transform.parent.GetComponent<AudioSource>().Play();
				}
				
			}
		}
   }

	private void OnEnterCoffin(int ID) {
		Debug.Log("La calavera ha entrado en el Coffin: " + ID);
		SkullIntoZone = ID;

	}

	private void OnExitCoffin(int ID) {
		Debug.Log("La calavera ha salido del Coffin: " + ID);
		SkullIntoZone = -1;
	}

	private void OnSelectSkull() {
		Debug.Log("La calavera ha sido cogida");
		SkullSelect = true;
	}

	private void OnDeselectSkull() {
		Debug.Log("La calavera ha sido soltada");
		SkullSelect = false;
	}

	IEnumerator MoveCoffin(Transform t, int count) {

		if (count > 0f) {
			t.Rotate(0f, 1f, 0f);
			yield return new WaitForSeconds(0.1f);
			StartCoroutine(MoveCoffin(t, --count));
		} else {
			yield return null;
		}
	}
}
